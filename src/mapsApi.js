const request = require('https');
const { response, json } = require('express');
const { Console } = require('console');
const { rejects } = require('assert');
const { resolve } = require('path');
//(TO-DO POS TESTE: Trocar por node-fetch ou request)
// REMOVER CONSOLE LOGS

const key = "AIzaSyA6TfU84r6wT2gu1NYAOCN7JkO342K21So";
const url = "https://maps.googleapis.com/maps/api/";

module.exports = {

  getLocationAndGas: function (lat, lng) {
    let data = '';
    // DOC PROMISE  https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Promise
    return new Promise((resolve, reject) => {
      request.get(url + 'geocode/json?latlng=' + lat + ',' + lng + '&sensor=false&key=' + key, (resp) => {

        resp.on('data', (chunk) => {
          data += chunk;
        });
        // End indica o final do recebimento dos dados
        resp.on('end', () => {
          const objFinal = { lat, lng };
          let dataJson = JSON.parse(data);
          objFinal.endereco = this.formatLocation(dataJson);
          resolve(objFinal) //Callback da promise https://stackoverflow.com/questions/41470296/how-to-await-and-return-the-result-of-a-http-request-so-that-multiple-request
        })
      })
    })
  },

  formatLocation: function (data) {
    // Primeira posicao do array Results e a Lat e Lng EXATA
    const { address_components: addresses } = data.results[0];
    let obj = {}
    addresses.map((address) => {
      const { short_name: value, types } = address;
      const typeFiltered = types.filter(t => t !== 'political');
      const [type] = typeFiltered; //desestruturando um array -> typefiltered == [0]
      obj = Object.assign(obj, { [`${type}`]: value }) //template string

    })
    const { street_number: numero,
      route: logradouro,
      sublocality: bairro,
      administrative_area_level_1: estado,
      administrative_area_level_2: cidade,
      postal_code: cep } = obj;

    return {
      estado,
      cidade,
      bairro,
      logradouro,
      numero,
      cep
    }
  },

  getGasStation: async function (lat, lng, token) {
    let data = '';

    // console.log('TOKEN_URL', `${url}place/search/json?location=${lat},${lng}&radius=10000&type=gas_station&key=${key}&pagetoken${token ? token : ''}`);
    // Testando a URL. 

    return new Promise((resolve, reject) => {
      // Utiliza query param fields? na api google? perguntar sobre 
      request.get(`${url}place/search/json?location=${lat},${lng}&radius=10000&type=gas_station&key=${key}&pagetoken${token ? token : ''}`, (resp) => {

        resp.on('data', (chunk) => {
          data += chunk;
        });

        resp.on('end', () => {
          let dataJson = JSON.parse(data);
          resolve(dataJson)
        })
      });
    });

  },

  getAllGasStation: async function (lat, lng,) {
    let arrayGasStations = [];
    const objFinal = {};

    gasStationData = await this.getGasStation(lat, lng);
    arrayGasStations.push(gasStationData.results);

    while (gasStationData.next_page_token != undefined) {
      // Invalid Response se fizer requests muito rapidas google api -> utilizei uma promisse para fazer o sleep
      await new Promise(resolve => setTimeout(resolve, 1300));
      gasStationData = await this.getGasStation(lat, lng, `=${gasStationData.next_page_token}`)
      arrayGasStations.push(gasStationData.results);
    }

    if (gasStationData.next_page_token == undefined && arrayGasStations.length >= 1) {
      return this.formatGasStation(arrayGasStations[1])
    }

  },

  formatGasStation: function (arrayGasStations) {
    const stations = []

    arrayGasStations.map((gasStation) => {
      let obj = {}
      //Melhorar com desestrutruracao no momento filtrando com if mesm
      if (gasStation.opening_hours != undefined && gasStation.opening_hours.open_now != false) {
        const { vicinity: endereco, geometry, name: nome } = gasStation

        obj = Object.assign(obj, { 'lat': geometry.location.lat, 'lng': geometry.location.lng, nome, endereco })

        stations.push(obj)
      }
    });
    return stations
  }
}