//  Lucas Uesato 23/06 -> 
// Test Backend da pixter, OBS: nao pode utilizar bibliotecas Framework livre

const express = require('express');
const { request, response } = require('express');
const mapsApi = require('./mapsApi');

const app = express();

app.use(express.json());

function logRequests(request, response, next) {
  const { method, url } = request;

  const log = `[${method}] ${url}`;

  console.time(log);
  next();
  console.timeEnd(log);
}

function validateLatLng(request, response, next) {
  const { lat, lng } = request.body;

  if (lat < -90 || lat > 90) {
    return response.status(400).json({ "error": "Invalid latitude" });
  } else if (lng < -180 || lng > 180) {
    return response.status(400).json({ "error": "Invalid longitude" });
  } else if (lat == "" || lng == "") {
    return response.status(400).json({ "error": "One or more fields empty" });
  } else {
    return next();
  }

}

app.use(logRequests);

app.get('/gas', validateLatLng,  async (request, response) => {
  const { lat, lng } = request.body; //desestruturacao de obj 

  Promise.all([await mapsApi.getLocationAndGas(lat, lng) , await mapsApi.getAllGasStation(lat, lng)]).then((values) => {
     
    const responseFinal = values[0]
    responseFinal.postos = values[1]
    return response.json(responseFinal);
  })
})

app.listen(5000, () => {
  console.log('App Running on 5000 ✌')
});